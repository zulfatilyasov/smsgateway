AppDispatcher = require '../AppDispatcher.coffee'
ContactConstants = require '../constants/ContactConstants.coffee'
GroupConstants = require '../constants/GroupConstants.coffee'
apiClient = require '../services/apiclient.coffee'
async = require 'async'

ContactActions = 
    contactsLoaded : false
    clean: ->
        @contactsLoaded = false
        AppDispatcher.handleViewAction
            actionType: ContactConstants.CLEAN

    createGroup:(name) ->
        apiClient.getUserGroups userId
            .then (resp) ->
                    groups = resp.body
                    AppDispatcher.handleViewAction
                        actionType: GroupConstants.RECEIVED_ALL_GROUPS
                        groups: groups
                , (err) ->
                    AppDispatcher.handleViewAction
                        actionType: GroupConstants.GET_ALL_GROUPS_FAIL
                        error: err

        AppDispatcher.handleViewAction
            actionType: GroupConstants.GET_GROUPS

    aggregateGroups:()->
        AppDispatcher.handleViewAction
            actionType: ContactConstants.AGGREGATE_GROUPS

    searchContacts:(query)->
      console.log query
      apiClient.searchContacts(query)
        .then (resp) ->
          contacts = resp.body.contacts
          console.log contacts
          AppDispatcher.handleViewAction
              actionType: ContactConstants.SEARCH_CONTACTS
              contacts: contacts
        , (err) ->
          console.log err

    triggerChange:()->
        AppDispatcher.handleViewAction
            actionType: ContactConstants.TRIGGER_CHANGE

    getAddressList:()->
      apiClient.getAddressList()
        .then (resp) ->
          addresses = resp.body.addresses
          AppDispatcher.handleViewAction
            actionType: ContactConstants.GET_ADDRESSLIST_SUCCESS
            addresses: addresses

    getUserVariables:(userId) ->
      apiClient.getUserVariables(userId)
        .then (resp) ->
          variables = resp.body
          AppDispatcher.handleViewAction
              actionType: ContactConstants.GET_VARIABLES_SUCCESS
              variables: variables
        , (err) ->
          AppDispatcher.handleViewAction
              actionType: ContactConstants.GET_VARIABLES_FAIL
              error: err

    createContactVariable:(variable) ->
      apiClient.createContactVariable(variable)
        .then (resp)->
          savedVariable = resp.body
          AppDispatcher.handleViewAction
              actionType: ContactConstants.CREATE_VARIABLE_SUCCESS
              variable: savedVariable
        , (err) ->
          AppDispatcher.handleViewAction
              actionType: ContactConstants.CREATE_VARIABLE_FAIL
              error: err

    replaceGroups:(targetGroups, sourceGroups) ->
      _.map targetGroups, (group) ->
        if group.id is null
          matchingGroup =_.first _.filter(sourceGroups, name:group.name)
          group.id = matchingGroup.id
        id:group.id
        name:group.name

    selectSingle: (id) ->
      AppDispatcher.handleViewAction
        actionType: ContactConstants.SELECT_CONTACT
        id: id

    selectAllItems: (value) ->
      AppDispatcher.handleViewAction
          actionType: ContactConstants.SELECT_ALL_CONTACTS
          value: value

    deleteContacts: (contactIds)->
      apiClient.deleteContacts contactIds
        .then (resp) ->
                AppDispatcher.handleViewAction
                    actionType: ContactConstants.DELETED_CONTACTS
                    contactIds: contactIds
            , (err) ->
                AppDispatcher.handleViewAction
                    actionType: ContactConstants.DELETE_CONTACTS_FAIL
                    contactIds: contactIds

    clearEdit:() ->
      AppDispatcher.handleViewAction
          actionType: ContactConstants.CLEAR_EDITED_CONTACT

    editContact: (contact) ->
      AppDispatcher.handleViewAction
          actionType: ContactConstants.EDIT_CONTACT
          contact: contact

    createMultipleGroups: (userId, groups, cb)->
      apiClient.createUserGroups userId, groups
        .then (resp) ->
          createdGroups = resp.body
          AppDispatcher.handleViewAction
            actionType: GroupConstants.SAVE_GROUP_SUCCESS
            groups: createdGroups
          cb createdGroups

    createMultipleContacts: (contacts, groups, userId) ->
      contacts = _.map contacts,  (c) ->
        c.groups = groups
        return c

      contactChunks = [contacts]

      i = 0
      skip =0
      limit = 500
      max = limit

      nextChunk = ->
        contactChunks[i] = contacts.slice(skip, max)
        skip += limit
        i++
        max = limit * (i+1)

      if contacts.length > 500
        nextChunk() while i*limit < contacts.length

      console.log contactChunks

      async.eachSeries contactChunks
      , (chunk, cb) ->
        apiClient.import(chunk)
          .then (resp) ->
            console.log 'saved ' + chunk.length
            savedContacts = resp.body.contacts
            cb(null, resp)
            AppDispatcher.handleViewAction
              actionType: ContactConstants.SAVE_CHUNK_SUCCESS
              savedCount: chunk.length
          , (err) ->
            cb(err)
            AppDispatcher.handleViewAction
              actionType: ContactConstants.CREATE_MULTIPLE_FAIL
              error: err
      , (err, results) -> 
        AppDispatcher.handleViewAction
          actionType: ContactConstants.CREATE_MULTIPLE_SUCCESS
        if err 
          console.log err
        console.log 'finished import'

    importContacts: (contacts, groups, userId) ->
      newGroups = _.filter groups, id:null
      if newGroups.length
        @createMultipleGroups userId, newGroups, (createdGroups) ->
          groups = ContactActions.replaceGroups groups, createdGroups
          ContactActions.createMultipleContacts(contacts, groups, userId)
      else
        ContactActions.createMultipleContacts(contacts, groups, userId)

      AppDispatcher.handleViewAction
        actionType: ContactConstants.IMPORT_CONTACTS
        totalCount: contacts.length

    resetImportMessages:->
      AppDispatcher.handleViewAction
        actionType: ContactConstants.RESET_IMPORT_MESSAGES

    saveContact: (contact) ->
      newGroups = _.filter contact.groups, id:null
      if newGroups.length
        @createMultipleGroups contact.userId, newGroups, (createdGroups) ->
          contact.groups = ContactActions.replaceGroups contact.groups, createdGroups
          ContactActions.createContact contact
      else
        contact.groups = ContactActions.replaceGroups contact.groups
        ContactActions.createContact contact

      AppDispatcher.handleViewAction
        actionType: ContactConstants.SAVE
        contact: contact

    createContact: (contact) ->
      apiClient.saveContact(contact.userId, contact)
        .then (resp) ->
          savedContact = resp.body
          savedContact.new = if contact.id then false else true
          AppDispatcher.handleViewAction
            actionType: ContactConstants.SAVE_SUCCESS
            contact: savedContact
        , (err) ->
          AppDispatcher.handleViewAction
            actionType: ContactConstants.SAVE_FAIL
            error: err

    getUserGroups: (userId) ->
        apiClient.getUserGroups userId
            .then (resp) ->
                    groups = resp.body
                    AppDispatcher.handleViewAction
                        actionType: GroupConstants.RECEIVED_ALL_GROUPS
                        groups: groups
                , (err) ->
                    AppDispatcher.handleViewAction
                        actionType: GroupConstants.GET_ALL_GROUPS_FAIL
                        error: err

        AppDispatcher.handleViewAction
            actionType: GroupConstants.GET_GROUPS

    deleteGroup: (userId, groupId, deleteContacts) ->
      apiClient.deleteGroup userId, groupId, deleteContacts
        .then (resp) ->
                AppDispatcher.handleViewAction
                    actionType: GroupConstants.GROUP_DELETED
                    groupId: groupId
            , (err) ->
                AppDispatcher.handleViewAction
                    actionType: GroupConstants.GROUP_DELETE_FAILED
                    error: err

    updateContactGroups: (contacts, selectedGroups, aggregatedGroups) ->
      groupsToAddTo = []
      groupsToRemoveFrom = []
      _.forEach selectedGroups, (group) ->
        _.forEach contacts, (contact) ->
          if not _.any(contact.groups, id:group.id)
            groupsToAddTo.push group.id
            contact.groups.push
              name:group.name
              id:group.id
      _.forEach aggregatedGroups, (group) ->
        if not _.any(selectedGroups, id:group.id)
          groupsToRemoveFrom.push group.id
          for contact in contacts
            _.remove(contact.groups, id:group.id)
      @submitContacts contacts
      contactIds = _.pluck contacts, 'id'
      @updateGroups contactIds, groupsToAddTo, groupsToRemoveFrom

    updateGroups: (contactsIds, groupsToAddTo, groupsToRemoveFrom) ->
      apiClient.updateContactsGroups contactsIds, groupsToAddTo, groupsToRemoveFrom
        .then (resp) ->
          console.log resp
        , (err) ->
          console.log err

    submitGroups:(groups) ->
      apiClient.updateMultipleGroups groups
        .then (resp) ->
          AppDispatcher.handleViewAction
            actionType: GroupConstants.UPDATED_GROUPS
            groups: groups
        , (err) ->
          console.log err

    submitContacts: (contacts) ->
      contacts = _.map contacts, (c) -> _.omit c, ['checked', 'key', 'new']
      apiClient.updateMultipleContacts contacts
        .then (resp) ->
          AppDispatcher.handleViewAction
            actionType: ContactConstants.UPDATED_CONTACTS
            contacts: contacts
        , (err) ->
          console.log err

    filter:(filters) ->
      apiClient.filterContacts(filters)
        .then (resp) ->
          contacts = resp.body.contacts
          console.log contacts
          AppDispatcher.handleViewAction
            actionType: ContactConstants.FILTER_SUCCESS
            contacts: contacts

        , (err) ->
          AppDispatcher.handleViewAction
            actionType: ContactConstants.FILTER_FAIL
            contacts: contacts

        AppDispatcher.handleViewAction
          actionType: ContactConstants.FILTER

    checkNewGroupsAndUpdateContacts:(userId, contacts, selectedGroups, newGroups, aggregatedGroups) ->
      if newGroups.length
        @createMultipleGroups userId, newGroups, (createdGroups) =>
          selectedGroups = selectedGroups.concat createdGroups
          @updateContactGroups contacts, selectedGroups, aggregatedGroups
      else
        @updateContactGroups contacts, selectedGroups, aggregatedGroups

    getUserContacts: (userId, groupId, skip, limit=50) ->
        if @contactsLoaded
            console.log 'contacts already loaded'
            return

        if skip?
          AppDispatcher.handleViewAction
            actionType: ContactConstants.GET_CONTACTS_PORTION
            skip:skip
            limit:limit
          return 

        apiClient.getUserContacts userId, groupId
            .then (resp) ->
                    contacts = resp.body.contacts or resp.body
                    AppDispatcher.handleViewAction
                        actionType: ContactConstants.RECEIVED_ALL_CONTACTS
                        contacts: contacts
                        limit:limit
                        groupId:groupId
                        isGroupContacts: if groupId then true else false
                    @contactsLoaded = true
                , (err) ->
                    AppDispatcher.handleViewAction
                        actionType: ContactConstants.GET_ALL_CONTACTS_FAIL
                        error: err

        AppDispatcher.handleViewAction
            actionType: ContactConstants.GET_CONTACTS

module.exports = ContactActions