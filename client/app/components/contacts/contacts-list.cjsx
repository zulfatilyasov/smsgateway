React = require 'react'
ContactItem = require './contact-item.cjsx'
contactActions = require '../../actions/ContactActions.coffee'
userActions = require '../../actions/UserActions.coffee'
contactStore = require '../../stores/ContactStore.coffee'
userStore = require '../../stores/UserStore.coffee'
ReactPaginate = require('react-paginate')
_ = require 'lodash'
ui = require '../../services/ui.coffee'
State = require('react-router').state
InfiniteScroll = require('react-infinite-scroll')(React)

_animationOff = false
animateItems = ->
    delay = if _animationOff  then 0 else 20
    $('.animated').each (i, el) ->
        unless $(@).is('.active')
            $(@)
                .css("-webkit-transition-delay", i*delay+'ms')
                .css("-o-transition-delay", i*delay+'ms')
                .css("transition-delay", i*delay+'ms')
                .addClass('active')

getState = ->
    contacts: contactStore.ContactList()
    pageCount: contactStore.pageCount()
    loading: contactStore.InProgress()
    hasMore: true

ContactList = React.createClass

    mixins: [ State ]

    getInitialState: ->
        getState()

    componentDidMount: ->
        $('.toobarWrap').scrollToFixed()
        contactStore.addChangeListener(@onChange)
        userId = userStore.userId();
        groupId = @props.params.groupId;
        if userStore.isAuthenticated()
            contactActions.getUserContacts(userId, groupId)
            contactActions.getUserGroups(userId);
        else
            userActions.logout()
        animateItems()

    componentWillUnmount: ->
        contactStore.removeChangeListener(@onChange)
        contactActions.clean()

    onChange: ->
        @setState getState()

    componentDidUpdate: (prevProps, prevState) ->
        animateItems()
        if $('.pager').css('position') isnt 'fixed'
            $('.pager').scrollToFixed bottom:0
        parentWidth = $('.pager').parent().width()
        $('.pager').css('width':parentWidth)
        @appendingContacts = false

    handlePageClick:(data)->
        userId = userStore.userId();
        groupId = @props.params.groupId;
        contactActions.getUserContacts(userId, groupId, data.selected * 50);

    render: ->
        ContactList = <div>
                        {
                            for contact in @state.contacts
                                contact.key = contact.id
                                <ContactItem {...contact} />
                        }
                      </div>
        <div>
            {
                if @state.contacts.length
                    <div>
                        {ContactList}
                    </div>
                else
                    if @state.loading
                        <div className="no-messages">Loading...</div>
                    else 
                        <div className="no-messages">No contacts</div>
            }
            <div>
            </div>
            {
                if @state.pageCount > 1
                    <div>
                        <ReactPaginate previousLabel={"previous"}
                           nextLabel={"next"}
                           breakLabel={<li className="break"><a href="">...</a></li>}
                           pageNum={@state.pageCount}
                           marginPagesDisplayed={2}
                           pageRangeDisplayed={5}
                           clickCallback={this.handlePageClick}
                           containerClassName={"pager"}
                           subContainerClassName={"pages pagination"}
                           activeClass={"active"} />
                    </div>
            }
        </div>

module.exports = ContactList