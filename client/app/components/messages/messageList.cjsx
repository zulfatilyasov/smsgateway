React = require 'react'
MessageItem = require './message-item.cjsx'
messageStore = require '../../stores/MessageStore.es6'
userStore = require '../../stores/UserStore.coffee'
messageActions = require '../../actions/MessageActions.coffee'
ReactPaginate = require 'react-paginate'
ui = require '../../services/ui.coffee'

_animationOff = false
animateItems = ->
    delay = if _animationOff then 0 else 20
    $('.animated').each (i, el) ->
        unless $(@).is('.active')
            $(@)
                .css("-webkit-transition-delay", i*delay+'ms')
                .css("-o-transition-delay", i*delay+'ms')
                .css("transition-delay", i*delay+'ms')
                .addClass('active')

unMounted = true
MessageList = React.createClass
    getInitialState: ->
        loadingContacts:false
        pageCount:messageStore.PageCount


    componentDidMount: ->
        unMounted = false
        $('.toobarWrap').scrollToFixed()
        animateItems()
        messageStore.addChangeListener @onChange

    componentWillUnmount: ->
        unMounted = true
        messageStore.removeChangeListener @onChange

    onChange: ->
        @setState
            pageCount:messageStore.PageCount

    componentDidUpdate: (prevProps, prevState) ->
        animateItems()
        if $('.pager').css('position') isnt 'fixed'
            $('.pager').scrollToFixed bottom:0
        parentWidth = $('.pager').parent().width()
        $('.pager').css('width':parentWidth)

        @appendingContacts = false

    handlePageClick:(data)->
        userId = userStore.userId()
        messageActions.getUserMessages(userId, messageStore.CurrentSection, data.selected * 50)

    render: ->
        <div>
            {
                for msg in @props.messages
                    msg.key = msg.id
                    <MessageItem {...msg} />
            }
            <div>
                {
                    <div className="loading">Loading...</div>
                }
            </div>
            {
                if @state.pageCount > 1
                    <div>
                        <ReactPaginate previousLabel={"previous"}
                           nextLabel={"next"}
                           breakLabel={<li className="break"><a href="">...</a></li>}
                           pageNum={@state.pageCount}
                           marginPagesDisplayed={2}
                           pageRangeDisplayed={5}
                           clickCallback={this.handlePageClick}
                           containerClassName={"pager"}
                           subContainerClassName={"pages pagination"}
                           activeClass={"active"} />
                    </div>
            }
        </div>

    

module.exports = MessageList