# Sms gateway


This project uses [Loopback.io](http://docs.strongloop.com/display/public/LB/LoopBack) node.js framework on the backend with Mongodb connector.  Client app is built with [React.js](http://facebook.github.io/react/) and [Flux](https://facebook.github.io/flux/docs/todo-list.html). 

Front end compilation and build process is implemented with [Webpack](http://webpack.github.io/) module bundler
###Folder stucture
Project structure is a standart loopback.io generated [project structure](http://docs.strongloop.com/display/public/LB/Project+layout+reference;jsessionid=2D147080B420B9209A2C5E7EB6D727FF).  
```
server/                   --> Node application scripts and configuration files.
  server.js               --> Main application program file.
  boot/                   --> Scripts to perform initialization and setup.
	  boot.js             --> Application models configuration and extensions.
  io/                     --> Scripts implementing socket.io messaging.
  jobs/                   --> agenda job definitions (scheduling messages)
  views/                  --> server side rendered ejs views
  config.*.json           --> Application settings for production and development
  
client/                   --> Client JavaScript, HTML, and CSS files.
	app/                  --> Client application source files
	   actions/           --> Flux Actions
	   components/        --> React.js components
	   constants/         --> Flux Constants
	   services/          --> Custom client scripts, helpers and validators
	     apiClient.coffee --> Module defining backend api calls.
	   stores/            --> Flux Stores
	build/                --> Compiled client app files, ready to deploy.
	master.jsx            --> Application layout.
	app-routes.jsx        --> Application routes.
	
common/
      models/            --> Loopback.io data model definitions.
            message.json   --> Message models properties and settings.
            message.js     --> Message model methods and operation hooks.
            
config/                  --> Capistrano deployment configuration files

webpack.config.js        --> Webpack configuration file for development
webpack.production.config.js --> Webpack config file for production
```
###Local installation 

1. Install MongoDb http://docs.mongodb.org/manual/installation/
2. Install Redis http://redis.io/topics/quickstart
3. Install Node.js https://nodejs.org/download/
4. Install git http://git-scm.com/download/mac
5. Clone the repo `git clone git@bitbucket.org:zulfatilyasov/smsgateway.git`
6. `cd smsgateway`
7. `npm install`

### Running application locally
1. `npm start` to start node.js application. server runs on port 3200 by default
2. open new terminal window
3. `npm run start-client` to start webpack dev server
4. navigate to http://localhost:3100

### Build and deploy
1. `npm run build` -- starts webapck build process for production
2.  push your changes to the repo.   
3. `cap production deploy` -- deploys application to remote server (130.211.140.186). You need to have ssh access to server if you want to deploy with capistrano.

###API
All available api methods can be viewed and tested via api explorer http://localhost:3200/explorer

[How to user api explorer](http://docs.strongloop.com/display/public/LB/Use+API+Explorer)