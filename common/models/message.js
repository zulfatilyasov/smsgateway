'use strict';
var messenger = require('../../server/io/messenger.coffee');
var consts = require('./messageConstants.coffee');
var MessageHelpers = require('./messageHelpers.coffee');
var loopback = require('loopback');
var _ = require('lodash');
var agenda = require('../../server/jobs/agenda.js');
function notAutorizedError() {
    var err = new Error('not authenticated');
    err.statusCode = 401;
    return err;
}

module.exports = function(Message) {
    var msgHelpers = new MessageHelpers(Message);
    var checkMessageIsSent = function(messageId) {
        setTimeout(function() {
            Message.findById(messageId, function(err, message) {
                if (!err && message) {
                    if (message.status === 'sending') {
                        message.updateAttribute('status', 'failed');
                    }
                }
            });
        }, 40 * 1000);
    };

    Message.observe('after save', function(ctx, next) {
        if (!ctx.instance) {
            next();
            return;
        }

        var userId = ctx.instance.userId;
        if (ctx.instance && ctx.isNewInstance) {
            if (ctx.instance.origin === 'web') {
                messenger.sendMessageToUserMobile(userId, ctx.instance);
            }

            if (ctx.instance.origin === 'mobile') {
                messenger.sendMessageToUserWeb(userId, ctx.instance);
            }
        }

        if (ctx.instance && !ctx.isNewInstance) {
            messenger.updateUserMessageOnWeb(userId, ctx.instance);
            console.log('socket io emitted update-message to web: %s#%s', userId, ctx.instance.body);
        }
        next();
    });

    Message.beforeRemote('create', function(ctx, message, next) {
        var ObjectId = Message.app.dataSources['Mongodb'].ObjectID;
        if (ctx.req.accessToken) {
            var userId = ctx.req.accessToken.userId;
            ctx.req.body.userId = new ObjectId(userId);
            next();
        } else {
            next(new Error('must be logged in to save message'));
        }
    });

    Message.deleteMany = function(selection, cb) {
        var ctx = loopback.getCurrentContext();
        var token = ctx && ctx.get('accessToken');
        if (!(token && token.userId)) {
            return cb(notAutorizedError());
        } else {
            var query = {
                id: {
                    inq: selection.includedIds
                },
                userId: token.userId
            };

            if(selection.selectAll){
                query = {
                    id: {
                        nin: selection.excludedIds
                    },
                    userId: token.userId
                };
            }

            return Message.destroyAll(query, function(err, info) {
                if (err) {
                    return cb(err);
                } else {
                    return cb(null);
                }
            });
        }
    };

    Message.remoteMethod('send', {
        accepts: [{
            arg: 'message',
            type: 'object'
        }, {
            arg: 'contacts',
            type: 'array'
        }, {
            arg: 'groups',
            type: 'array'
        }],

        returns: {
            arg: 'messages',
            type: 'array'
        },

        http: {
            path: '/send',
            verb: 'post'
        }
    });

    var sendMessageToContactsAndGroups = function(message, contacts, groupIds, cb) {
        var contactIds = _.map(contacts, function(c) {
            return c.id;
        });

        var Group = Message.app.models.Group;
        var Contact = Message.app.models.Contact;

        Group.find({
            fields: {
                contacts: true
            },
            where: {
                id: {
                    inq: groupIds
                }
            }
        }, function(err, groups) {
            if (err) {
                cb(err);
                return;
            }
            var groupContactIds = [];
            for (var i = groups.length - 1; i >= 0; i--) {
                groupContactIds = groupContactIds.concat(groups[i].contacts);
            }
            var allContactsIds = groupContactIds.concat(contactIds);
            Contact.find({
                where: {
                    id: {
                        inq: allContactsIds
                    }
                }
            }, function(err, recipients) {
                if (err) {
                    cb(err);
                    return;
                }

                var newMessages = _.map(recipients, function(recipient) {
                    var msg = _.cloneDeep(message);
                    msg.address = recipient.phone;
                    return msg;
                });

                Message.create(newMessages, function(err, result) {
                    if (err) {
                        cb(err);
                        return;
                    }
                    udpateLastContactedDate(recipients);
                    cb(null, result);
                });
            });
        });
    };

    function udpateLastContactedDate(contacts) {
        var ids = _.pluck(contacts, 'id');
        var ObjectId = Message.app.dataSources['Mongodb'].ObjectID;
        var objectIds = [];
        for (var i = ids.length - 1; i >= 0; i--) {
            objectIds.push(new ObjectId(ids[i]));
        }

        var ContactCollection = Message.dataSource.connector.db.collection('Contact');
        ContactCollection.update({
            _id:{
                '$in': objectIds
            }
        },{
            '$set':{
                lastContacted: new Date()
            }
        }, {multi:true});
    }

    function saveScheduled(message, cb) {
        if (!message.sendDate) {
            var error = new Error('Scheduled message should have send date');
            error.statusCode = 401;
            cb(error);
            return;
        }

        Message.create(message, function(err, message) {
            if (err) {
                cb(err);
                return;
            }

            agenda.schedule(message.sendDate, 'send message', {
                id: message.id
            });

            cb(message);
        });
    }

    Message.createContactsAndSend = function(message, contacts, groups, cb) {
        var newContacts = _.filter(contacts, {
            id: null
        });

        var existingContacts = _.filter(contacts, function(c) {
            return !!c.id;
        });

        var groupIds = _.pluck(groups, 'id');
        var Contact = Message.app.models.Contact;

        if (newContacts.length) {
            Contact.create(newContacts, function(err, createdContacts) {
                if (err) {
                    cb(err);
                    return;
                }
                var contacts = existingContacts.concat(createdContacts);
                sendMessageToContactsAndGroups(message, contacts, groupIds, cb);
            });
        } else {
            sendMessageToContactsAndGroups(message, contacts, groupIds, cb);
        }
    };

    Message.send = function(message, contacts, groups, cb) {
        if (message.status === 'scheduled') {
            message.address = {
                contacts: contacts,
                groups: groups
            };
            saveScheduled(message, cb);
        } else {
            Message.createContactsAndSend(message, contacts, groups, cb);
        }
    };


    Message.remoteMethod('deleteMany', {
        accepts: {
            arg: 'selection',
            type: 'object'
        },
        http: {
            path: '/delete_many',
            verb: 'post'
        }
    });

    Message.remoteMethod('getCount', {
        accepts: {
            arg: 'section',
            type: 'string'
        },
        http: {
            path: '/get_count',
            verb: 'get'
        },
        returns:{
            arg: 'count',
            type: 'number'
        }
    });

    Message.getCount = function (section, cb) {
        var ctx = loopback.getCurrentContext();
        var token = ctx && ctx.get('accessToken');
        if (!(token && token.userId)) {
            return cb(notAutorizedError());
        }

        var where = {userId: token.userId};
        if (section === 'outcoming')
           where['outcoming']=true;
        if (section === 'incoming')
           where['incoming']=true;
        if (section === 'starred')
           where['starred']=true;
        if (section === 'sent')
           where['status']='sent';
        if (section === 'failed')
           where['status']='failed';
        if (section === 'queued')
           where['status']='queued';
        if (section === 'cancelled')
           where['status']='cancelled';

        Message.count(where, function (err, count) {
            if(err) return cb(err);
            cb(null, count);
        });
    };

    Message.resend = function(selection, cb) {
        msgHelpers.getUserMessagesByIds(selection, function(err, oldMessages) {
            if (err) {
                console.log(err);
                cb(err);
            } else {
                msgHelpers.resendMessages(oldMessages, function(err, newMessages) {
                    if (err) {
                        console.log(err);
                        cb(err);
                    } else {
                        var result = [];
                        if(newMessages && newMessages.length){
                            result = newMessages.slice(0, 100);
                        }
                        cb(null, result);
                    }
                });
            }
        });
    };

    Message.remoteMethod('resend', {
        accepts: {
            arg: 'selection',
            type: 'object'
        },
        http: {
            path: '/resend',
            verb: 'post'
        },
        returns: {
            arg: 'messages',
            type: 'array'
        }
    });

    Message.cancel = function(selection, cb) {
        msgHelpers.setCancelled(selection, cb);
    };

    Message.remoteMethod('cancel', {
        accepts: {
            arg: 'selection',
            type: 'object'
        },
        http: {
            path: '/cancel',
            verb: 'post'
        },
        returns: {
            arg: 'messages',
            type: 'array'
        }
    });

    Message.sendQueued = function(userId, cb) {
        console.log('sending queued');
        msgHelpers.getUserMessagesByStatus(userId, 'queued', function(err, messages) {
            console.log(messages);
            if (err) {
                console.log(err);
                cb(err);
            } else {
                for (var i = messages.length - 1; i >= 0; i--) {
                    messenger.sendMessageToUserMobile(messages[i].userId, messages[i]);
                }
            }
        });
    };
};
